﻿using BepInEx;
using BepInEx.Configuration;
using System.Text.RegularExpressions;

namespace Reconstituent {
	public static class Configuration {
		public static ConfigFile Content;
		
		public static void Init() {
			Content = new ConfigFile(Paths.ConfigPath + "\\Reconstituent-Content.cfg", true);
		}

		public static ConfigEntry<T> Bind<T>(string key, T defaultValue, string description = "") {
			key = Regex.Replace(key.Trim(), @"[=\n\t\'\[\]\\]", "");
			description = Regex.Replace(description.Trim(), @"<.*?>", "");
			return Content.Bind<T>(key, key, defaultValue, description); ;
		}
	}
}
