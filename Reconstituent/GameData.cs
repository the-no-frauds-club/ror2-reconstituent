﻿using HG;
using R2API;
using RoR2;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Reconstituent {
	public class EliteDefinition {
		public EliteDef Elite;
		public BuffDef Buff;
		public EquipmentDef Equipment;
	}

	public static class GameData {
		public static AssetBundle ReconstituentOne;

		public static class Elite {
			public static EliteDefinition Bombastic;
			public static EliteDefinition Hypocrite;
			public static EliteDefinition Siphoned;
			public static EliteDefinition Oozing;
		};

		public static class Items {
			// Replacement item declarations \\

			// White
			public static ItemDef BundleOfFireworks;

			// Green
			public static ItemDef SquidPolyp;

			// New item declarations \\

			// White - Void
			public static ItemDef WidowsWardrobe;
			public static ItemDef BrightsideBomb;
			public static ItemDef MildMolasses;
			public static ItemDef StrigasGem;
			public static ItemDef SpentCheddar;

			// Green
			public static ItemDef KnockoutDriver;

			// Green - Void
			public static ItemDef CrowCall;
			public static ItemDef HollowedHead;

			// Red
			public static ItemDef CosmicCarcass;
			public static ItemDef DerelictJawbone;

			// Lunar
		//  public static ItemDef HistoricQuill;
		};
	
		public static Dictionary<string, string> ShaderLookup = new Dictionary<string, string>() {
			{"stubbed hopoo games/deferred/standard", "shaders/deferred/hgstandard"},
			{"stubbed hopoo games/fx/cloud intersection remap", "shaders/fx/hgintersectioncloudremap" },
			{"stubbed hopoo games/fx/cloud remap", "shaders/fx/hgcloudremap" },
			{"stubbed hopoo games/fx/distortion", "shaders/fx/hgdistortion" },
			{"stubbed hopoo games/deferred/snow topped", "shaders/deferred/hgsnowtopped" },
			{"stubbed hopoo games/fx/solid parallax", "shaders/fx/hgsolidparallax" }
		};

		public static void MapStubbedShaders(AssetBundle assetBundle) {
			var materialAssets = assetBundle.LoadAllAssets<Material>().Where(material => material.shader.name.StartsWith("Stubbed"));
			foreach (Material material in materialAssets) {
				var newShader = LegacyResourcesAPI.Load<Shader>(ShaderLookup[material.shader.name.ToLowerInvariant()]);
				if (newShader) {
					material.shader = newShader;
				}
			}
		}

		public static AssetBundle LoadAssetBundle(string path) {
			AssetBundle bundle = null;
			using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(path)) {
				bundle = AssetBundle.LoadFromStream(stream);
			}
			if (bundle) {
				MapStubbedShaders(bundle);
			}
			return bundle;
		}

		public static BuffDef CreateBuff(string id, Color color, bool stackable = true, bool debuff = false, string icon = "") {
			BuffDef buffDef = ScriptableObject.CreateInstance<BuffDef>();

			string pluginPrefix = ReconstituentPlugin.PluginName.ToUpper() + "_";
			buffDef.name = pluginPrefix + id;
			buffDef.buffColor = color;
			buffDef.canStack = stackable;
			buffDef.isDebuff = debuff;
			if (icon.Length > 0) {
				buffDef.iconSprite = Resources.Load<Sprite>(icon);
			}
			else {
				buffDef.iconSprite = Resources.Load<Sprite>("Textures/MiscIcons/texMysteryIcon");
			}

			ContentAddition.AddBuffDef(buffDef);
			return buffDef;
		}

		public static EliteDef CreateElite(string id, string modifier, Color color = default, EquipmentDef equipment = null, float healthBoost = 1f, float damageBoost = 1f) {
			EliteDef eliteDef = ScriptableObject.CreateInstance<EliteDef>();

			string pluginPrefix = ReconstituentPlugin.PluginName.ToUpper() + "_";
			string name = pluginPrefix + id;
			string modifierToken = name + "_MODIFIER";

			eliteDef.name = name;
			eliteDef.modifierToken = modifierToken;
			eliteDef.color = color;
			eliteDef.eliteEquipmentDef = equipment;
			eliteDef.healthBoostCoefficient = healthBoost;
			eliteDef.damageBoostCoefficient = damageBoost;

			LanguageAPI.Add(modifierToken, modifier);

			ContentAddition.AddEliteDef(eliteDef);
			return eliteDef;
		}

		public static EquipmentDef CreateEquipment(string id, float cooldown = 0,
			string name = "", string pickup = "", string description = "", string lore = "",
			AssetBundle bundle = null, string icon = "", string prefab = "",
			BuffDef buff = null, UnlockableDef unlock = null,
			bool canDrop = true, bool canRandom = true, bool isBoss = false, bool isLunar = false) {
			var enabled = Configuration.Bind<bool>(name, true, description);
			if (!enabled.Value) {
				return null;
			}

			EquipmentDef equipmentDef = ScriptableObject.CreateInstance<EquipmentDef>();

			string pluginPrefix = ReconstituentPlugin.PluginName.ToUpper() + "_";
			string nameStart = pluginPrefix + id;
			string nameToken = nameStart + "_NAME";
			string pickupToken = nameStart + "_PICKUP";
			string descriptionToken = nameStart + "_DESC";
			string loreToken = nameStart + "_LORE";

			equipmentDef.name = nameStart;
			equipmentDef.nameToken = nameToken;
			equipmentDef.pickupToken = pickupToken;
			equipmentDef.descriptionToken = descriptionToken;
			equipmentDef.loreToken = loreToken;

			LanguageAPI.Add(nameToken, name);
			LanguageAPI.Add(pickupToken, pickup);
			LanguageAPI.Add(descriptionToken, description);
			LanguageAPI.Add(loreToken, lore);

			if (icon.Length > 0) {
				if (bundle) {
					equipmentDef.pickupIconSprite = bundle.LoadAsset<Sprite>(icon);
				}
				else {
					equipmentDef.pickupIconSprite = Resources.Load<Sprite>(icon);
				}
			}
			else {
				equipmentDef.pickupIconSprite = Resources.Load<Sprite>("Textures/MiscIcons/texMysteryIcon");
			}

			if (prefab.Length > 0) {
				if (bundle) {
					equipmentDef.pickupModelPrefab = bundle.LoadAsset<GameObject>(prefab);
				}
				else {
					equipmentDef.pickupModelPrefab = Resources.Load<GameObject>(prefab);
				}
			}
			else {
				equipmentDef.pickupModelPrefab = Resources.Load<GameObject>("Prefabs/PickupModels/PickupMystery");
			}

			equipmentDef.cooldown = cooldown;
			if (buff) {
				equipmentDef.passiveBuffDef = buff;
			}
			if (unlock) {
				equipmentDef.unlockableDef = unlock;
			}

			equipmentDef.canDrop = canDrop;
			equipmentDef.canBeRandomlyTriggered = canRandom;
			equipmentDef.isBoss = isBoss;
			equipmentDef.isLunar = isLunar;

			// Potentially deprecated
			equipmentDef.enigmaCompatible = true;
			equipmentDef.appearsInMultiPlayer = true;
			equipmentDef.appearsInSinglePlayer = true;

			ContentAddition.AddEquipmentDef(equipmentDef);
			return equipmentDef;
		}

		// Call from Awake
		// Tier1 = white, Tier2 = green, Tier3 = red, Boss = yellow, Lunar = lunar, NoTier = helper, Void prefixes some tiers
		// canRemove determines if a shrine of order or a printer can take this item
		// hidden means that there will be no pickup notification, and it won't appear in the inventory at the top of the screen
		public static ItemDef CreateItem(string id, ItemTier tier, ItemTag[] tags = null,
			string name = "", string pickup = "", string description = "", string lore = "",
			AssetBundle bundle = null, string icon = "", string prefab = "",
			string childName = "", Vector3 pos = default, Vector3 scale = default, Vector3 angle = default,
			bool canRemove = true, bool hidden = false) {
			var enabled = Configuration.Bind<bool>(name + " (" + tier.ToString() + ")", true, description);
			if (!enabled.Value) {
				return null;
			}

			ItemDef itemDef = ScriptableObject.CreateInstance<ItemDef>();
			itemDef.tier = tier;
			itemDef.tags = tags;
			itemDef.canRemove = canRemove;
			itemDef.hidden = hidden;

			string pluginPrefix     = ReconstituentPlugin.PluginName.ToUpper() + "_";
			string nameStart        = pluginPrefix + id;
			string nameToken        = nameStart + "_NAME";
			string pickupToken      = nameStart + "_PICKUP";
			string descriptionToken = nameStart + "_DESC";
			string loreToken        = nameStart + "_LORE";

			itemDef.name = nameStart;
			itemDef.nameToken = nameToken;
			itemDef.pickupToken = pickupToken;
			itemDef.descriptionToken = descriptionToken;
			itemDef.loreToken = loreToken;

			LanguageAPI.Add(nameToken, name);
			LanguageAPI.Add(pickupToken, pickup);
			LanguageAPI.Add(descriptionToken, description);
			LanguageAPI.Add(loreToken, lore);

			if (icon.Length > 0) {
				if (bundle) {
					itemDef.pickupIconSprite = bundle.LoadAsset<Sprite>(icon);
				} else {
					itemDef.pickupIconSprite = Resources.Load<Sprite>(icon);
				}
			} else {
				itemDef.pickupIconSprite = Resources.Load<Sprite>("Textures/MiscIcons/texMysteryIcon");
			}

			if (prefab.Length > 0) {
				if (bundle) {
					itemDef.pickupModelPrefab = bundle.LoadAsset<GameObject>(prefab);
				} else {
					itemDef.pickupModelPrefab = Resources.Load<GameObject>(prefab);
				}
			} else {
				itemDef.pickupModelPrefab = Resources.Load<GameObject>("Prefabs/PickupModels/PickupMystery");
			}

			ItemDisplayRule[] itemDisplayRules = null;
			if (childName.Length > 0) {
				itemDisplayRules = new ItemDisplayRule[1];
				itemDisplayRules[0].childName   = childName;
				itemDisplayRules[0].localPos    = pos;
				itemDisplayRules[0].localScale  = scale;
				itemDisplayRules[0].localAngles = angle;
			}

			// Add item to API
			// We might consider using ContentAddition instead. -P
			ItemAPI.Add(new CustomItem(itemDef, itemDisplayRules));
			return itemDef;
		}

		public static void RemoveItem(ItemDef itemDef) {
			itemDef.tier = ItemTier.NoTier;
			itemDef.hidden = true;
		}

		// Call from RoR2Application.onLoad
		public static ItemDef ReplaceItem(ItemDef oldItemDef) {
			ItemDef itemDef = ScriptableObject.CreateInstance<ItemDef>();

			itemDef.name = oldItemDef.name;
			itemDef.nameToken = oldItemDef.nameToken;
			itemDef.pickupToken = oldItemDef.pickupToken;
			itemDef.descriptionToken = oldItemDef.descriptionToken;
			itemDef.loreToken = oldItemDef.loreToken;
			itemDef.tags = oldItemDef.tags;

			itemDef.tier = oldItemDef.tier;
			itemDef.canRemove = oldItemDef.canRemove;
			itemDef.hidden = oldItemDef.hidden;

			itemDef.pickupIconSprite = oldItemDef.pickupIconSprite;
			itemDef.pickupModelPrefab = oldItemDef.pickupModelPrefab;

			ItemDisplayRule[] itemDisplayRules = null; // TODO: Copy old item display rules

			// Add item to API
			ItemAPI.Add(new CustomItem(itemDef, itemDisplayRules));

			RemoveItem(oldItemDef);
			return itemDef;
		}

		// Call from RoR2Application.onLoad
		public static void UpdateItem(ItemDef itemDef, ItemTier tier = ItemTier.NoTier, ItemTag[] tags = null,
			string name = "", string pickup = "", string description = "", string lore = "", bool canRemove = true) {
			if (tier != ItemTier.NoTier) {
				itemDef.tier = tier;
			}
			string pluginPrefix = ReconstituentPlugin.PluginName.ToUpper() + "_";
			if (name.Length > 0) {
				string nameToken = pluginPrefix + itemDef.nameToken;
				itemDef.nameToken = nameToken;
				LanguageAPI.Add(nameToken, name);
			}
			if (pickup.Length > 0) {
				string pickupToken = pluginPrefix + itemDef.pickupToken;
				itemDef.pickupToken = pickupToken;
				LanguageAPI.Add(pickupToken, pickup);
			}
			if (description.Length > 0) {
				string descriptionToken = pluginPrefix + itemDef.descriptionToken;
				itemDef.descriptionToken = descriptionToken;
				LanguageAPI.Add(descriptionToken, description);
			}
			if (lore.Length > 0) {
				string loreToken = pluginPrefix + itemDef.loreToken;
				itemDef.loreToken = loreToken;
				LanguageAPI.Add(loreToken, lore);
			}
			if (tags != null) {
				itemDef.tags = tags;
			}

			if (canRemove == false) {
				itemDef.canRemove = false;
			}
			else {
				itemDef.canRemove = true;
			}
		}

		// Call from On.RoR2.Items.ContagiousItemManager.Init
		public static void RegisterTransmutation(ItemDef from, ItemDef to) {
			ItemDef.Pair pair = new ItemDef.Pair{ itemDef1 = from, itemDef2 = to };
			var values = ItemCatalog.itemRelationships[DLC1Content.ItemRelationshipTypes.ContagiousItem];
			ArrayUtils.ArrayAppend(ref values, pair);
			ItemCatalog.itemRelationships[DLC1Content.ItemRelationshipTypes.ContagiousItem] = values;
		}
	}
}
