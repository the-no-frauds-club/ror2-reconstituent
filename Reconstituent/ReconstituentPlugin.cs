using BepInEx;
using R2API;
using R2API.Utils;
using RoR2;
using RoR2.Projectile;
using RoR2.Orbs;
using EntityStates.Huntress.HuntressWeapon;
using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections.Generic;
using System.Security.Permissions;
using System.Linq;

[assembly: SecurityPermission(SecurityAction.RequestMinimum, SkipVerification = true)]
namespace Reconstituent {

	[BepInDependency(R2API.R2API.PluginGUID)]
	[BepInPlugin(PluginGUID, PluginName, PluginVersion)]
	[R2APISubmoduleDependency(nameof(ItemAPI), nameof(LanguageAPI), nameof(RecalculateStatsAPI), nameof(EliteAPI), nameof(CharacterBody))]
	[NetworkCompatibility(CompatibilityLevel.EveryoneMustHaveMod, VersionStrictness.EveryoneNeedSameModVersion)]
	public class ReconstituentPlugin : BaseUnityPlugin {
		public const string PluginGUID = PluginAuthor + "." + PluginName;
		public const string PluginAuthor = "FraudsClub";
		public const string PluginName = "Reconstituent";
		public const string PluginVersion = "0.0.1";

		public void Awake() {
			Log.Init(Logger);
			Configuration.Init();

			// Items
			//GameData.Items.BundleOfFireworks = GameData.ReplaceItem(RoR2Content.Items.Firework);
			//GameData.Items.SquidPolyp = GameData.ReplaceItem(RoR2Content.Items.Squid);
			RegisterNewItems();
			RoR2Application.onLoad += () => {
				UpdateItems();
			};
			On.RoR2.Items.ContagiousItemManager.Init += (orig) => {
				Log.LogInfo("Registering Transmutations");

				// White - Rotting
				GameData.RegisterTransmutation(RoR2Content.Items.Syringe, GameData.Items.WidowsWardrobe);
				GameData.RegisterTransmutation(RoR2Content.Items.StunChanceOnHit, GameData.Items.BrightsideBomb);
				GameData.RegisterTransmutation(RoR2Content.Items.SprintBonus, GameData.Items.MildMolasses);
				GameData.RegisterTransmutation(RoR2Content.Items.NearbyDamageBonus, GameData.Items.StrigasGem);
				GameData.RegisterTransmutation(DLC1Content.Items.GoldOnHurt, GameData.Items.SpentCheddar);

				// Green - Rotting
				GameData.RegisterTransmutation(RoR2Content.Items.EnergizedOnEquipmentUse, GameData.Items.CrowCall);
				GameData.RegisterTransmutation(RoR2Content.Items.DeathMark, GameData.Items.HollowedHead);

				orig();
			};

			// New logic

			ThrowGlaive.glaiveTravelSpeed *= 2;

			On.RoR2.HuntressTracker.OnEnable += (orig, self) => {
				self.indicator.active = false;
			};

			On.RoR2.HuntressTracker.SearchForTarget += (orig, self, aimRay) => {
				self.search.teamMaskFilter = TeamMask.GetUnprotectedTeams(self.teamComponent.teamIndex);
				self.search.filterByLoS = true;
				self.search.searchOrigin = aimRay.origin;
				self.search.searchDirection = aimRay.direction;
				self.search.sortMode = BullseyeSearch.SortMode.Angle;
				self.search.maxDistanceFilter = self.maxTrackingDistance * 1.5f;
				self.search.maxAngleFilter = self.maxTrackingAngle;
				self.search.RefreshCandidates();
				self.search.FilterOutGameObject(self.gameObject);
				self.trackingTarget = self.search.GetResults().FirstOrDefault<HurtBox>();
			};

            On.RoR2.Skills.HuntressTrackingSkillDef.HasTarget += (orig, skill) => {
				if (skill == skill.characterBody.skillLocator.secondary) {
					return orig(skill);
                }
				return true;
            };

			On.EntityStates.Huntress.HuntressWeapon.FireSeekingArrow.FireOrbArrow += (orig, self) => {
				if (self.firedArrowCount >= self.maxArrowCount || self.arrowReloadTimer > 0f || !NetworkServer.active) {
					return;
				}
				self.firedArrowCount++;
				self.arrowReloadTimer = self.arrowReloadDuration;

				float speed = 800f;
				if (self is FireFlurrySeekingArrow) {
					speed = 400f;
                }

				Ray aimRay = self.GetAimRay();
				ProjectileManager.instance.FireProjectile(FireArrow.projectilePrefab,
					aimRay.origin, Util.QuaternionSafeLookRotation(aimRay.direction),
					self.gameObject, self.characterBody.damage * self.orbDamageCoefficient,
					20f, self.isCrit, DamageColorIndex.Default, null, speed);
			};

			On.EntityStates.Huntress.BlinkState.FixedUpdate += (orig, self) => {
				if (self is not EntityStates.Huntress.MiniBlinkState) {
					self.speedCoefficient = 20f;
					self.duration = 0.15f;
				}

				orig(self);
			};

			On.EntityStates.Huntress.BlinkState.OnExit += (orig, self) => {
				orig(self);

				self.characterBody.AddTimedBuff(RoR2Content.Buffs.Cloak, 5f);
				self.characterBody.AddTimedBuff(RoR2Content.Buffs.CloakSpeed, 5f);
			};

			// Disable drop pod!!!
			On.RoR2.Stage.RespawnCharacter += (orig, self, character) => {
				self.usePod = false;
				orig(self, character);
            };

			/*
			On.RoR2.CharacterBody.OnSkillActivated += (orig, self, skill) => {
				if (skill.isCombatSkill) {
					self.SetBuffCount(RoR2Content.Buffs.Cloak.buffIndex, 0);
					self.SetBuffCount(RoR2Content.Buffs.CloakSpeed.buffIndex, 0);
                }
				if (self.inventory) {
					if (skill == self.skillLocator.secondary) {
						int firework_count = self.inventory.GetItemCount(GameData.Items.BundleOfFireworks);
						if (firework_count > 0) {
							Transform child = self.gameObject.GetComponent<ModelLocator>()?.modelTransform?.GetComponent<ChildLocator>()?.FindChild("FireworkOrigin");
							Vector3 position2 = (bool)child ? child.position : self.gameObject.transform.position + Vector3.up * 2f;
							int num = 4 + firework_count * 4;
							FireworkLauncher component2 = Instantiate<GameObject>(LegacyResourcesAPI.Load<GameObject>("Prefabs/FireworkLauncher"), position2, Quaternion.identity).GetComponent<FireworkLauncher>();
							component2.owner = self.gameObject;
							component2.crit = Util.CheckRoll(self.crit, self.master);
							component2.remaining = num;
						}
					} else if (skill == self.skillLocator.special) {
						int squid_count = self.inventory.GetItemCount(GameData.Items.SquidPolyp);
						if (squid_count > 0) {
							DirectorSpawnRequest directorSpawnRequest = new DirectorSpawnRequest((SpawnCard)LegacyResourcesAPI.Load<CharacterSpawnCard>("SpawnCards/CharacterSpawnCards/cscSquidTurret"), new DirectorPlacementRule() {
								placementMode = DirectorPlacementRule.PlacementMode.Approximate,
								minDistance = 5f,
								maxDistance = 25f,
								position = self.gameObject.transform.position
							}, RoR2Application.rng);
							directorSpawnRequest.teamIndexOverride = new TeamIndex?(TeamIndex.Player);
							directorSpawnRequest.summonerBodyObject = self.gameObject;
							directorSpawnRequest.onSpawnedServer += (result) => {
								if (!result.success) {
									return;
								}
								CharacterMaster component3 = result.spawnedInstance.GetComponent<CharacterMaster>();
								component3.inventory.GiveItem(RoR2Content.Items.BoostAttackSpeed, 10 * (squid_count - 1));
							};
							DirectorCore.instance.TrySpawnObject(directorSpawnRequest);
						}
					}
				}
				orig(self, skill);
			};

			On.RoR2.CharacterBody.RecalculateStats += (orig, self) => {
				orig(self);

				// TODO: Replace whole item
				int hoofCount = self.inventory.GetItemCount(RoR2Content.Items.Hoof);
				if (hoofCount > 0) {
					self.moveSpeed += 0.01f * hoofCount;
				}

				// Disable on-hit RNG
				self.bleedChance = 0;
				self.baseCrit = 0;
				self.crit = 0;
			};			
			*/

			// Delicate Watch rework - Return consumed watches to active at stage start
			On.RoR2.CharacterBody.Start += (orig, self) => {
				orig(self);
				if (self.inventory) {
					int watches = self.inventory.GetItemCount(DLC1Content.Items.FragileDamageBonusConsumed);
					if (watches > 0) {
						self.inventory.GiveItem(DLC1Content.Items.FragileDamageBonus, watches);
						self.inventory.RemoveItem(DLC1Content.Items.FragileDamageBonusConsumed, watches);
						CharacterMasterNotificationQueue.PushItemTransformNotification(self.master, DLC1Content.Items.FragileDamageBonusConsumed.itemIndex, DLC1Content.Items.FragileDamageBonus.itemIndex, CharacterMasterNotificationQueue.TransformationType.Default);
						EffectData effectData2 = new EffectData {
							origin = self.transform.position
						};
						effectData2.SetNetworkedObjectReference(self.gameObject);
						EffectManager.SpawnEffect(HealthComponent.AssetReferences.fragileDamageBonusBreakEffectPrefab, effectData2, true);
					}
				}
			};

			// Disable vanilla Old War Stealthkit behavior
			Log.LogInfo("Registering Events");
			On.RoR2.Items.PhasingBodyBehavior.FixedUpdate += (orig, self) => {
				// Do nothing, handled in onCharacterDeathGlobal
			};

			/*
			// Power Elixir rework
			// TODO: move this to seperate function
			On.RoR2.HealthComponent.UpdateLastHitTime += (orig, self, damageValue, damagePosition, damageIsSilent, attacker) => {
				if (NetworkServer.active && self.body && damageValue > 0f) {
					if (self.itemCounts.medkit > 0) {
						self.body.AddTimedBuff(RoR2Content.Buffs.MedkitHeal, 2f);
					}
					if (self.itemCounts.healingPotion > 0 && self.combinedHealthFraction <= Math.Max(0.01, 0.3 - 0.01 * self.itemCounts.healingPotion)) {
						self.HealFraction(0.75f, default(ProcChainMask));
						EffectData effectData = new EffectData {
							origin = self.transform.position
						};
						effectData.SetNetworkedObjectReference(self.gameObject);
						EffectManager.SpawnEffect(LegacyResourcesAPI.Load<GameObject>("Prefabs/Effects/HealingPotionEffect"), effectData, true);
					}
					// Delicate Watch rework - Consume only one item per hit
					if (self.itemCounts.fragileDamageBonus > 0 && self.isHealthLow) {
						self.body.inventory.GiveItem(DLC1Content.Items.FragileDamageBonusConsumed, 1);
						self.body.inventory.RemoveItem(DLC1Content.Items.FragileDamageBonus, 1);
						CharacterMasterNotificationQueue.PushItemTransformNotification(self.body.master, DLC1Content.Items.FragileDamageBonus.itemIndex, DLC1Content.Items.FragileDamageBonusConsumed.itemIndex, CharacterMasterNotificationQueue.TransformationType.Default);
						EffectData effectData2 = new EffectData {
							origin = self.transform.position
						};
						effectData2.SetNetworkedObjectReference(self.gameObject);
						EffectManager.SpawnEffect(HealthComponent.AssetReferences.fragileDamageBonusBreakEffectPrefab, effectData2, true);
					}
				}
				if (damageIsSilent) {
					return;
				}
				self.lastHitTime = Run.FixedTimeStamp.now;
				self.lastHitAttacker = attacker;
				self.serverDamageTakenThisUpdate += damageValue;
				if (self.modelLocator) {
					Transform modelTransform = self.modelLocator.modelTransform;
					if (modelTransform) {
						Animator component = modelTransform.GetComponent<Animator>();
						if (component) {
							string layerName = "Flinch";
							int layerIndex = component.GetLayerIndex(layerName);
							if (layerIndex >= 0) {
								component.SetLayerWeight(layerIndex, 1f + Mathf.Clamp01(damageValue / self.fullCombinedHealth * 10f) * 3f);
								component.Play("FlinchStart", layerIndex);
							}
						}
					}
				}
				IPainAnimationHandler painAnimationHandler = self.painAnimationHandler;
				if (painAnimationHandler == null) {
					return;
				}
				painAnimationHandler.HandlePain(damageValue, damagePosition);
			};
			*/

			Log.LogInfo("Registering Global Events");
			GlobalEventManager.onServerDamageDealt += OnServerDamageDealt;
			GlobalEventManager.onCharacterDeathGlobal += OnCharacterDeathGlobal;

			Log.LogInfo(nameof(Awake) + " done.");
		}

        public void OnServerDamageDealt(DamageReport report) {
			// Check if attacker was world
			if (!report.attacker || !report.attackerBody) {
				return;
			}

			CharacterBody attacker = report.attackerBody;
			if (attacker.inventory) {
				// Crow Call
				int crow_call_count = attacker.inventory.GetItemCount(GameData.Items.CrowCall);
				if (crow_call_count > 0) {
					attacker.inventory.DeductActiveEquipmentCooldown(0.4f + 0.1f * crow_call_count);
				}
			}
		}

		public void OnCharacterDeathGlobal(DamageReport report) {
			// Check if attacker was world
			if (!report.attacker || !report.attackerBody) {
				return;
			}
			CharacterBody attacker = report.attackerBody;
			if (attacker.inventory) {
				// New War Stealthkit
				int item_count = attacker.inventory.GetItemCount(RoR2Content.Items.Phasing);
				if (item_count > 0) {
					attacker.AddTimedBuff(RoR2Content.Buffs.Cloak, item_count * 0.25f);
				}
				/*
				item_count = attacker.inventory.GetItemCount(GameData.Items.CosmicCarcass);
				if (item_count > 0) {

				}
				*/
			}
		}

		public void Update() { // Every frame
			if (PlayerCharacterMasterController.instances.Count == 1 && PlayerCharacterMasterController.instances[0].master.GetBodyObject()) {
				var transform = PlayerCharacterMasterController.instances[0].master.GetBodyObject().transform;
				if (Input.GetKey(KeyCode.LeftControl)) {
					if (Input.GetKeyDown(KeyCode.F1)) {
						PickupDropletController.CreatePickupDroplet(PickupCatalog.itemTierToPickupIndex[ItemTier.VoidTier1], transform.position, transform.forward * 20f);
					}
					if (Input.GetKeyDown(KeyCode.F2)) {
						PickupDropletController.CreatePickupDroplet(PickupCatalog.itemTierToPickupIndex[ItemTier.VoidTier2], transform.position, transform.forward * 20f);
					}
					if (Input.GetKeyDown(KeyCode.F3)) {
						PickupDropletController.CreatePickupDroplet(PickupCatalog.itemTierToPickupIndex[ItemTier.VoidTier3], transform.position, transform.forward * 20f);
					}
					if (Input.GetKeyDown(KeyCode.F4)) {
						PickupDropletController.CreatePickupDroplet(PickupCatalog.itemTierToPickupIndex[ItemTier.VoidBoss], transform.position, transform.forward * 20f);
					}
				} else {
					if (Input.GetKeyDown(KeyCode.F1)) {
						PickupDropletController.CreatePickupDroplet(PickupCatalog.itemTierToPickupIndex[ItemTier.Tier1], transform.position, transform.forward * 20f);
					}
					if (Input.GetKeyDown(KeyCode.F2)) {
						PickupDropletController.CreatePickupDroplet(PickupCatalog.itemTierToPickupIndex[ItemTier.Tier2], transform.position, transform.forward * 20f);
					}
					if (Input.GetKeyDown(KeyCode.F3)) {
						PickupDropletController.CreatePickupDroplet(PickupCatalog.itemTierToPickupIndex[ItemTier.Tier3], transform.position, transform.forward * 20f);
					}
					if (Input.GetKeyDown(KeyCode.F4)) {
						PickupDropletController.CreatePickupDroplet(PickupCatalog.itemTierToPickupIndex[ItemTier.Boss], transform.position, transform.forward * 20f);
					}
				}
				if (Input.GetKeyDown(KeyCode.F5)) {
					PickupDropletController.CreatePickupDroplet(PickupCatalog.itemTierToPickupIndex[ItemTier.Lunar], transform.position, transform.forward * 20f);
				}
				if (Input.GetKeyDown(KeyCode.F6)) {
					PickupDropletController.CreatePickupDroplet(PickupCatalog.FindPickupIndex(RoR2Content.Equipment.CommandMissile.equipmentIndex), transform.position, transform.forward * 20f);
				}
				if (Input.GetKeyDown(KeyCode.F7)) {
					// TODO: Add Money
				}
			}
		}

		public void RegisterNewItems() {
			Log.LogInfo("Registering New Items");
			GameData.ReconstituentOne = GameData.LoadAssetBundle("Reconstituent.reconstituent_one");

			// White - Void \\
			GameData.Items.WidowsWardrobe = GameData.CreateItem("WIDOWS_WARDROBE",
				// Info
				tier: ItemTier.VoidTier1,
				tags: new[] {
					ItemTag.Damage,
				},
				name: "Widow's Wardrobe",
				pickup: "Gain an additional attack. <style=cIsVoid>Corrupts all Soldier's Syringes.</style>",
				description: "Grants an <style=cIsDamage>additional attack</style> that deals <style=cIsDamage>25% damage</style> (<style=cStack>+25% per stack</style>). At <style=cIsDamage>100% damage</style>, the next stack grants <style=cIsDamage>another attack</style>.\n<style=cIsVoid>Corrupts all Soldier's Syringes.</style>",
				lore: "Catharsis for those left behind.",

				// Assets
				bundle: GameData.ReconstituentOne,
				prefab: "Assets/Import/mesh_item/widows_wardrobe/mdl_widows_wardrobe.prefab",
				icon: "Assets/Import/icon/widows_wardrobe_icon.png",

				// Display Rules
				childName: "Chest",
				pos: new Vector3(-0.35f, -0.1f, 0f),
				scale: new Vector3(0.15f, 0.15f, 0.15f),
				angle: new Vector3(0f, 180f, 0f)
			);

			GameData.Items.BrightsideBomb = GameData.CreateItem("BRIGHTSIDE_BOMB",
				// Info
				tier: ItemTier.VoidTier1,
				tags: new[] {
					ItemTag.Utility,
				},
				name: "Brightside Bomb",
				pickup: "Flash enemies on critical strike. <style=cIsVoid>Corrupts all Dizzy Bombs.</style>",
				description: "<style=cIsUtility>Flashes</style> on <style=cIsDamage>critical strike</style> for <style=cIsUtility>0.25 seconds</style> (<style=cStack>+0.25s per stack</style>). <style=cIsUtility>Flashed enemies</style> always attack the nearest target, regardless of allegiance.\n<style=cIsVoid>Corrupts all Dizzy Bombs.</style>",
				lore: "One of few.",

				// Assets
				bundle: GameData.ReconstituentOne,
				prefab: "Assets/Import/mesh_item/brightside_bomb/mdl_brightside_bomb.prefab",
				icon: "Assets/Import/icon/brightside_bomb_icon.png",

				// Display Rules
				childName: "Chest",
				pos: new Vector3(-0.35f, -0.1f, 0f),
				scale: new Vector3(0.15f, 0.15f, 0.15f),
				angle: new Vector3(0f, 180f, 0f)
			);

			GameData.Items.MildMolasses = GameData.CreateItem("MILD_MOLASSES",
				// Info
				tier: ItemTier.VoidTier1,
				tags: new[] {
					ItemTag.Utility,
				},
				name: "Mild Molasses",
				pickup: "Slow nearby enemies while sprinting. <style=cIsVoid>Corrupts all Energy Drinks.</style>",
				description: "While <style=cIsUtility>sprinting</style>, slow the <style=cIsDamage>movement and attack speeds</style> of all enemies in a <style=cIsUtility>5m radius</style> (<style=cStack>+1m per stack</style>) by <style=cIsUtility>5%</style> (<style=cStack>+5% per stack</style>).\n<style=cIsVoid>Corrupts all Dizzy Bombs.</style>",
				lore: "Wade to me, and die.",

				// Assets
				bundle: GameData.ReconstituentOne,
				prefab: "Assets/Import/mesh_item/mild_molasses/mdl_mild_molasses.prefab",
				icon: "Assets/Import/icon/mild_molasses_icon.png",

				// Display Rules
				childName: "Chest",
				pos: new Vector3(-0.35f, -0.1f, 0f),
				scale: new Vector3(0.15f, 0.15f, 0.15f),
				angle: new Vector3(0f, 180f, 0f)
			);

			GameData.Items.StrigasGem = GameData.CreateItem("STRIGAS_GEM",
				// Info
				tier: ItemTier.VoidTier1,
				tags: new[] {
					ItemTag.Utility,
				},
				name: "Striga's Gem",
				pickup: "Increase damage to distant targets. <style=cIsVoid>Corrupts all Focus Crystals.</style>",
				description: "Increase <style=cIsDamage>damage</style> to enemies beyond <style=cIsDamage>13m</style> by <style=cIsDamage>20%</style> (<style=cStack>+20% per stack</style>).",
				lore: "In all directions!",

				// Assets
				bundle: GameData.ReconstituentOne,
				prefab: "Assets/Import/mesh_item/strigas_gem/mdl_strigas_gem.prefab",
				icon: "Assets/Import/icon/strigas_gem_icon.png",

				// Display Rules
				childName: "Chest",
				pos: new Vector3(-0.35f, -0.1f, 0f),
				scale: new Vector3(0.15f, 0.15f, 0.15f),
				angle: new Vector3(0f, 180f, 0f)
			);

			GameData.Items.SpentCheddar = GameData.CreateItem("SPENT_CHEDDAR",
				// Info
				tier: ItemTier.VoidTier1,
				tags: new[] {
					ItemTag.Utility,
				},
				name: "Spent Cheddar",
				pickup: "Restore life by spending money. <style=cIsVoid>Corrupts all Rolls of Pennies.</style>",
				description: "Spending money <style=cIsHealing>restores life</style> at a rate of <style=cIsHealing>1 HP</style> (<style=cStack>+1 HP per stack</style>) for every dollar spent. <style=cIsVoid>Corrupts all Rolls of Pennies.</style>",
				lore: "Burning a hole in your pocket?",

				// Assets
				bundle: GameData.ReconstituentOne,
				prefab: "Assets/Import/mesh_item/spent_cheddar/mdl_spent_cheddar.prefab",
				icon: "Assets/Import/icon/spent_cheddar_icon.png",

				// Display Rules
				childName: "Chest",
				pos: new Vector3(-0.35f, -0.1f, 0f),
				scale: new Vector3(0.15f, 0.15f, 0.15f),
				angle: new Vector3(0f, 180f, 0f)
			);

			// Green \\
			GameData.Items.KnockoutDriver = GameData.CreateItem("KNOCKOUT_DRIVER",
				// Info
				tier: ItemTier.Tier2,
				tags: new[] {
					ItemTag.Damage,
				},
				name: "Knockout Driver",
				pickup: "Knock enemies back on critical strike.",
				description: "On <style=cIsDamage>critical strike</style>, knock enemies back by <style=cIsDamage>2m</style> (<style=cStack>+2m per stack</style>).",
				lore: "Where's the weigh-in?",

				// Assets
				bundle: GameData.ReconstituentOne,
				prefab: "Assets/Import/mesh_item/knockout_driver/mdl_knockout_driver.prefab",
				//  icon: "Assets/Import/icon/knockout_driver_icon.png",

				// Display Rules
				childName: "Chest",
				pos: new Vector3(-0.35f, -0.1f, 0f),
				scale: new Vector3(0.15f, 0.15f, 0.15f),
				angle: new Vector3(0f, 180f, 0f)
			);

			// Green - Void \\
			GameData.Items.CrowCall = GameData.CreateItem("CROW_CALL",
				// Info
				tier: ItemTier.VoidTier2,
				tags: new[] {
					ItemTag.Utility,
				},
				name: "Crow Call",
				pickup: "Reduce equipment cooldowns on hit. <style=cIsVoid>Corrupts all War Horns.</style>",
				description: "Whenever you <style=cIsDamage>hit an enemy</style>, reduce equipment cooldowns by <style=cIsUtility>1s</style> <style=cStack>(+0.5s per stack)</style>.\n<style=cIsVoid>Corrupts all War Horns.</style>",
				lore: "Squawk a triumphant reply!",

				// Assets
				bundle: GameData.ReconstituentOne,
				prefab: "Assets/Import/mesh_item/crow_call/mdl_crow_call.prefab",
				icon: "Assets/Import/icon/crow_call_icon.png",

				// Display Rules
				childName: "Chest",
				pos: new Vector3(-0.35f, -0.1f, 0f),
				scale: new Vector3(0.15f, 0.15f, 0.15f),
				angle: new Vector3(0f, 180f, 0f)
			);

			GameData.Items.HollowedHead = GameData.CreateItem("HOLLOWED_HEAD",
				// Info
				tier: ItemTier.VoidTier2,
				tags: new[] {
					ItemTag.Damage,
				},
				name: "Hollowed Head",
				pickup: "Consume your victims' debuffs to deal bonus damage. <style=cIsVoid>Corrupts all Death Mikes.</style>",
				description: "Landing an attack consumes one <style=cIsUtility>unique debuff</style>, dealing <style=cIsDamage>200% damage</style>. This damage is increased by <style=cIsDamage>5%</style> (<style=cStack>+5% per stack</style>) for each stack of the consumed debuff.\n<style=cIsVoid>Corrupts all Death Mikes.</style>",
				lore: "Always knew there was nothing in there.",

				// Assets
				bundle: GameData.ReconstituentOne,
				prefab: "Assets/Import/mesh_item/hollowed_head/mdl_hollowed_head.prefab",
				icon: "Assets/Import/icon/hollowed_head_icon.png",

				// Display Rules
				childName: "Chest",
				pos: new Vector3(-0.35f, -0.1f, 0f),
				scale: new Vector3(0.15f, 0.15f, 0.15f),
				angle: new Vector3(0f, 180f, 0f)
			);

			// Red \\

			GameData.Items.CosmicCarcass = GameData.CreateItem("COSMIC_CARCASS",
				// Info
				tier: ItemTier.Tier3,
				tags: new[] {
					ItemTag.Damage,
				},
				name: "Cosmic Carcass",
				pickup: "Trigger your on-kill effect an additional time.",
				description: "Your <style=cIsDamage>on-kill effects</style> trigger <style=cIsUtility>1 more time</style> (<style=cStack>+1 trigger per stack</style>).",
				lore: "Now, you are wrong twice.",

				// Assets
				bundle: GameData.ReconstituentOne,
				prefab: "Assets/Import/mesh_item/cosmic_carcass/mdl_cosmic_carcass.prefab",
				//  icon: "Assets/Import/icon/cosmic_carcass_icon.png",

				// Display Rules
				childName: "Chest",
				pos: new Vector3(-0.35f, -0.1f, 0f),
				scale: new Vector3(0.15f, 0.15f, 0.15f),
				angle: new Vector3(0f, 180f, 0f)
			);

			GameData.Items.DerelictJawbone = GameData.CreateItem("DERELICT_JAWBONE",
				// Info
				tier: ItemTier.Tier3,
				tags: new[] {
					ItemTag.Utility,
				},
				name: "Derelict Jawbone",
				pickup: "Reduce item cooldowns on critical strike.",
				description: "On <style=cIsDamage>critical strike</style>, reduce item cooldowns by <style=cIsUtility>1s</style> (<style=cStack>+1s per stack</style>).",
				lore: "Origin unknown.",

				// Assets
				bundle: GameData.ReconstituentOne,
				prefab: "Assets/Import/mesh_item/derelict_jawbone/mdl_derelict_jawbone.prefab",
				//  icon: "Assets/Import/icon/derelict_jawbone_icon.png",

				// Display Rules
				childName: "Chest",
				pos: new Vector3(-0.35f, -0.1f, 0f),
				scale: new Vector3(0.15f, 0.15f, 0.15f),
				angle: new Vector3(0f, 180f, 0f)
			);

			// Orange \\

			// New elites\\

			return;

			// Bombastic
			GameData.Elite.Bombastic.Buff = GameData.CreateBuff("BUFF_ELITE_BOMBASTIC", new Color(0.59f, 0.18f, 0.35f));
			GameData.Elite.Bombastic.Equipment = GameData.CreateEquipment("EQUIPMENT_ELITE_BOMBASTIC", 0,
				name: "The Iron Word",
				pickup: "You, the Bombastic.",
				description: "Your attacks explode in a 5m radius around the impact site.",
				lore: "They will hear you, always.",
				buff: GameData.Elite.Bombastic.Buff
			);
			GameData.Elite.Bombastic.Elite = GameData.CreateElite("ELITE_BOMBASTIC", 
				modifier: "Bombastic",
				color: new Color(0.59f, 0.18f, 0.35f),
				equipment: GameData.Elite.Bombastic.Equipment,
				healthBoost: 1.1f, 
				damageBoost: 1.5f
			);

			// Hypocrite
			GameData.Elite.Hypocrite.Buff = GameData.CreateBuff("BUFF_ELITE_HYPOCRITE", new Color(0.85f, 0.89f, 0.38f));
			GameData.Elite.Hypocrite.Equipment = GameData.CreateEquipment("EQUIPMENT_ELITE_HYPOCRITE", 0,
				name: "Shattered Silver",
				pickup: "You, the Hypocrite.",
				description: "You reflect incoming projectiles.",
				lore: "",
				buff: GameData.Elite.Hypocrite.Buff
			);
			GameData.Elite.Hypocrite.Elite = GameData.CreateElite("ELITE_HYPOCRITE", 
				modifier: "Hypocrite", 
				color: new Color(0.85f, 0.89f, 0.38f),
				equipment: GameData.Elite.Hypocrite.Equipment,
				healthBoost: 1.1f,
				damageBoost: 1.5f
			);

			// Siphoned
			GameData.Elite.Siphoned.Buff = GameData.CreateBuff("BUFF_ELITE_SIPHONED", new Color(1f, 0.25f, 0.25f));
			GameData.Elite.Siphoned.Equipment = GameData.CreateEquipment("EQUIPMENT_ELITE_SIPHONED", 0,
				name: "Maw of the Master",
				pickup: "You, the Siphoned.",
				description: "You heal for 50% of all damage dealt.",
				lore: "",
				buff: GameData.Elite.Siphoned.Buff
			);
			GameData.Elite.Siphoned.Elite = GameData.CreateElite("ELITE_SIPHONED",
				modifier: "Siphoned",
				color: new Color(1f, 0.25f, 0.25f),
				equipment: GameData.Elite.Siphoned.Equipment,
				healthBoost: 1.1f,
				damageBoost: 1.5f
			);

			// Oozing
			GameData.Elite.Oozing.Buff = GameData.CreateBuff("BUFF_ELITE_OOZING", new Color(0.07f, 0.28f, 0.09f));
			GameData.Elite.Oozing.Equipment = GameData.CreateEquipment("EQUIPMENT_ELITE_OOZING", 0,
				name: "Pustulent Polyp",
				pickup: "You, the Oozing.",
				description: "You leave a trail that slows enemy movement and attack speeds by 25%.",
				lore: "",
				buff: GameData.Elite.Oozing.Buff
			);
			GameData.Elite.Oozing.Elite = GameData.CreateElite("ELITE_OOZING",
				modifier: "Oozing",
				color: new Color(0.07f, 0.28f, 0.09f),
				equipment: GameData.Elite.Oozing.Equipment,
				healthBoost: 1.1f,
				damageBoost: 1.5f
			);
		}

		public void UpdateItems() {
			Log.LogInfo("Registering Modified Items");

			// White \\
			GameData.UpdateItem(RoR2Content.Items.Syringe,
				name: "Soldier's Syringe",
				pickup: "Attack faster.",
				description: "Increases <style=cIsDamage>attack speed</style> by <style=cIsDamage>15%</style> (<style=cStack>+15% per stack</style>).",
				lore: "Ah, that's the stuff.");

			GameData.UpdateItem(RoR2Content.Items.Tooth,
				name: "Monster Tooth",
				pickup: "Heal from corpses.",
				description: "Killing an enemy spawns a <style=cIsHealing>healing orb</style> that heals for <style=cIsHealing>8</style> plus an additional <style=cIsHealing>2%</style> (<style=cStack>+2% per stack</style>) of <style=cIsHealing>maximum health</style>.",
				lore: "Scavenge to survive.");

			GameData.UpdateItem(RoR2Content.Items.CritGlasses,
				name: "Shaped Lenses",
				pickup: "Deal more weak point damage.",
				description: "Deals <style=cIsDamage>10%</style> more <style=cIsDamage>weak point damage</style> (<style=cStack>+10% per stack</style>).\nIncreases <style=cIsDamage>weak point size</style> by 5% (<style=cStack>+5% per stack</style>).",
				lore: "See only the target.");

			GameData.UpdateItem(RoR2Content.Items.Hoof,
				name: "Paul's Goat Hoof",
				pickup: "Move faster.",
				description: "Makes you move <style=cIsUtility>15% faster</style> (<style=cStack>+15% per stack</style>).",
				lore: "In stride.");

			GameData.UpdateItem(RoR2Content.Items.Mushroom,
				name: "Bustling Fungus",
				pickup: "Heal when standing still.",
				description: "While standing still, creates a <style=cIsHealing>healing zone</style> that heals all allies within <style=cIsUtility>3m</style> (<style=cStack>+1.5m per stack</style>) for <style=cIsHealing>4.5% health</style> (<style=cStack>+2.25% per stack</style>).",
				lore: "Let nature take its course.");

			GameData.UpdateItem(RoR2Content.Items.Crowbar,
				name: "Crowbar",
				pickup: "Deal bonus damage to full-health enemies.",
				description: "Deal <style=cIsDamage>+75%</style> (<style=cStack>+75% per stack</style>) damage to enemies above <style=cIsDamage>90% health.</style>",
				lore: "This doesn't seem to caw.");

			GameData.UpdateItem(RoR2Content.Items.BleedOnHit,
				name: "Tri-Tip Dagger",
				pickup: "Make enemies bleed heavily.",
				description: "Provides <style=cIsDamage>20%</style> more <style=cIsDamage>bleed damage</style> (<style=cStack>+20% per stack</style>).",
				lore: "Cut deeper.");

			GameData.UpdateItem(RoR2Content.Items.WardOnLevel,
				name: "Warbanner",
				pickup: "",
				description: "On <style=cIsUtility>level up</style> or starting the <style=cIsUtility>Teleporter event</style>, drop a banner that strengthens all allies within <style=cIsUtility>16m</style> (<style=cStack>+8m per stack</style>).\nRaise <style=cIsDamage>attack</style> and <style=cIsUtility>movement speeds</style> by <style=cIsDamage>30%</style>. ",
				lore: "You carry the flag.",

				tier: ItemTier.NoTier
				);

			GameData.UpdateItem(RoR2Content.Items.HealWhileSafe,
				name: "Cautious Slug",
				pickup: "Heal while safe.",
				description: "Increases <style=cIsHealing>base health regeneration</style> by <style=cIsHealing>+3 hp/s</style> (<style=cStack>+3 hp/s per stack</style>) while out of danger for <style=cIsUtility>5 seconds</style>.",
				lore: "Come out, come out, whereever you are.");

			GameData.UpdateItem(RoR2Content.Items.PersonalShield,
				name: "Personal Shield Generator",
				pickup: "Gain a recharging shield.",
				description: "Gain a <style=cIsHealing>shield</style> equal to <style=cIsHealing>8%</style> (<style=cStack>+8% per stack</style>) of your maximum health. <style=cIsUtility>Recharges</style> when out of danger for <style=cIsUtility>2 seconds</style>.",
				lore: "Don't let your guard down.");

			GameData.UpdateItem(RoR2Content.Items.Medkit,
				name: "Medkit",
				pickup: "Receive a delayed heal after taking damage.",
				description: "2 seconds after receiving damage, <style=cIsHealing>heal</style> for <style=cIsHealing>20</style> plus an additional <style=cIsHealing>5%</style> (<style=cStack>+5% per stack</style>) of <style=cIsHealing>maximum health</style>. ",
				lore: "Stay frosty.");

			GameData.UpdateItem(RoR2Content.Items.IgniteOnKill,
				name: "Gasoline",
				pickup: "Ignite enemies near your victims.",
				description: "Killing an enemy <style=cIsDamage>ignites</style> all enemies within <style=cIsDamage>12m</style> (<style=cStack>+4m per stack</style>) for <style=cIsDamage>150% base damage</style>.\nEnemies burn for <style=cIsDamage>150%</style> (<style=cStack>+75% per stack</style>) base damage.",
				lore: "Need a light?");

			GameData.UpdateItem(RoR2Content.Items.StunChanceOnHit,
				name: "Dizzy Bomb",
				pickup: "Slow the attacks of your victims with each critical strike.",
				description: "Applies <style=cIsUtility>Dizzy</style> on <style=cIsDamage>critical strike</style>. <style=cIsUtility>Dizzy</style> enemies have their <style=cIsDamage>attack speed</style> reduced by <style=cIsUtility>10%</style> (<style=cStack>+5% per stack</style>)",
				lore: "One of many.");

			GameData.UpdateItem(RoR2Content.Items.Firework,
				name: "Bundle of Fireworks",
				pickup: "Launch fireworks with each critical strike.",
				description: "Landing a <style=cIsDamage>critical strike</style> launches <style=cIsDamage>8</style> (<style=cStack>+4 per stack</style>) <style=cIsDamage>fireworks</style> that deal <style=cIsDamage>100% base damage</style>.",
				lore: "Cause for celebration!");

			GameData.UpdateItem(RoR2Content.Items.SprintBonus,
				name: "Energy Drink",
				pickup: "Sprint faster.",
				description: "<style=cIsUtility>Sprint speed</style> is improved by <style=cIsUtility>25%</style> (<style=cStack>+25% per stack</style>).",
				lore: "Stay hydrated.");

			GameData.UpdateItem(RoR2Content.Items.SecondarySkillMagazine,
				name: "Backup Magazine",
				pickup: "Gain an extra charge of your Secondary skill.",
				description: "Add <style=cIsUtility>+1</style> (<style=cStack>+1 per stack</style>) charge of your <style=cIsUtility>Secondary skill</style>.",
				lore: "Always be prepared.");

			GameData.UpdateItem(RoR2Content.Items.StickyBomb,
				name: "Sticky Bomb",
				pickup: "Attach a bomb on critical strike.",
				description: "On <style=cIsDamage>critical strike</style>, attach a <style=cIsDamage>delayed bomb</style> to your victim that explodes in a <style=cIsDamage>10m</style> (<style=cStack>+2m per stack</style>) radius, dealing <style=cIsDamage>150%</style> (<style=cStack>+20% per stack</style>) <style=cIsDamage>base damage</style>.",
				lore: "Got a present for ya!");

			GameData.UpdateItem(RoR2Content.Items.TreasureCache,
				name: "Rusty Key",
				pickup: "Pair with a lockbox for a powerful item.",
				description: "A <style=cIsUtility>hidden cache</style> will appear in a random location <style=cIsUtility>on each stage</style>. Opening the cache <style=cIsUtility>consumes</style> this item. Powerful caches require <style=cIsUtility>three stacks</style> of this item.",
				lore: "What's in the box?");

			GameData.UpdateItem(RoR2Content.Items.BossDamageBonus,
				name: "Armor-Piercing Rounds",
				pickup: "Deal extra damage to bosses.",
				description: "Deal an additional <style=cIsDamage>20% damage</style> (<style=cStack>+20% per stack</style>) to bosses. ",
				lore: "The bigger they are...");

			GameData.UpdateItem(RoR2Content.Items.BarrierOnKill,
				name: "Topaz Brooch",
				pickup: "Gain a <style=cIsDamage>barrier</style> on <style=cIsDamage>kill</style>.",
				description: "Gain a <style=cIsHealing>temporary barrier</style> on kill for <style=cIsHealing>15 health</style> (<style=cStack>+15 per stack</style>).",
				lore: "They shall fuel you.");

			GameData.UpdateItem(RoR2Content.Items.NearbyDamageBonus,
				name: "Focus Crystal",
				pickup: "Deal bonus damage to nearby enemies.",
				description: "Increase damage to enemies within <style=cIsDamage>13m</style> by <style=cIsDamage>20%</style> (<style=cStack>+20% per stack</style>).",
				lore: "Just a little closer...");

			GameData.UpdateItem(RoR2Content.Items.FlatHealth,
				name: "Bison Steak",
				pickup: "Gain more maximum health.",
				description: "Increases <style=cIsHealing>maximum health</style> by <style=cIsHealing>25</style> (<style=cStack>+25 per stack</style>) and <style=cIsHealing>health regeneration</style> by <style=cIsHealing>+0.5</style> (<style=cStack>+0.1 per stack</style>).",
				lore: "Fit for a king's dinner!");

			GameData.UpdateItem(RoR2Content.Items.ArmorPlate,
				name: "Repulsion Armor Plate",
				pickup: "Block a flat amount of damage.",
				description: "Reduce all <style=cIsDamage>incoming damage</style> by <style=cIsDamage>5</style> (<style=cStack>+5 per stack</style>).\nCannot be reduced below <style=cIsDamage>1</style>.",
				lore: "They'll have to do better than that.");

			GameData.UpdateItem(DLC1Content.Items.AttackSpeedAndMoveSpeed,
				name: "Mocha",
				pickup: "Increase movement and attack speeds.",
				description: "Increases <style=cIsDamage>attack speed</style> and <style=cIsUtility>movement speed</style> by <style=cIsDamage>7.5%</style> (<style=cStack>+7.5 per stack</style>).",
				lore: "Just to help you get up in the morning.");

			GameData.UpdateItem(DLC1Content.Items.FragileDamageBonus,
				name: "Delicate Watch",
				pickup: "Deal more damage. Breaks at low health.",
				description: "Increase damage by <style=cIsDamage>20%</style> (<style=cStack>+20% per stack</style>). Taking damage when below <style=cIsHealth>25% health</style> breaks a <style=cStack>stack</style> of this item.",
				lore: "Handle with care.");

			GameData.UpdateItem(DLC1Content.Items.FragileDamageBonusConsumed,
				name: "Broken Watch",
				pickup: "It's still right twice a day!",
				description: "Reassembles between stages.",
				lore: "From timepiece to showpiece.");

			GameData.UpdateItem(DLC1Content.Items.OutOfCombatArmor,
				name: "Oddly-Shaped Opal",
				pickup: "Reduce damage from a single strike.",
				description: "Increase <style=cIsUtility>armor</style> by <style=cIsUtility>100</style> (<style=cStack>+100 per stack</style>) when out of danger for <style=cIsUtility>5 seconds</style>.",
				lore: "An oasis that walks with you.");

			GameData.UpdateItem(DLC1Content.Items.GoldOnHurt,
				name: "Roll of Pennies",
				pickup: "Gain gold when hit.",
				description: "Gain <style=cIsUtility>3</style> (<style=cStack>+3 per stack</style>) <style=cIsUtility>gold</style> on <style=cIsDamage>taking damage</style> from an enemy. <style=cIsUtility>Scales over time</style>.",
				lore: "At least take me out to dinner first!");

			// Green \\
			GameData.UpdateItem(RoR2Content.Items.Bear,
				tier: ItemTier.Tier2,
				name: "Innocent Eyes",
				pickup: "Reflect enemy attacks.",
				description: "<style=cIsDamage>Reflects</style> incoming projectiles for <style=cIsDamage>100% damage</style> (<style=cStack>+25% per stack</style>).",
				lore: "Back at ya!");

			GameData.UpdateItem(DLC1Content.Items.HealingPotion,
				tier: ItemTier.Tier2,
				name: "Power Elixir",
				pickup: "Drink, but do not die.",
				description: "When you drop below <style=cIsDamage>30% life</style> (<style=cStack>-1% life per stack</style>), heal to full life. Can not occur more than once per <style=cIsUtility>30s</style> <style=cStack>(-0.1s per stack)</style>.",
				lore: "A taste of the grave.");

			GameData.UpdateItem(RoR2Content.Items.Missile,
				name: "Underbarrel Launcher",
				pickup: "Launch missiles on critical strike.",
				description: "On <style=cIsDamage>critical strike</style>, launch a <stlye=cIsDamage>seaking missile</style> that deals <style=cIsDamage>250% damage</style> (<style=cStack>+250% per stack</style>).",
				lore: "See no evil.");

			GameData.UpdateItem(RoR2Content.Items.Phasing,
				name: "New War Stealthkit",
				pickup: "Cloak on kill.",
				description: "Whenever you <style=cIsDamage>kill an enemy</style>, cloak for <style=cIsUtility>0.25s</style> <style=cStack>(+0.25s per stack)</style>.",
				lore: "See no evil.");

			GameData.UpdateItem(RoR2Content.Items.DeathMark,
				name: "Death Mike",
				pickup: "Deal more damage to debuffed targets.",
				description: "Increase <style=cIsDamage>damage</style> by <style=cIsDamage>1%</style> (style=cStack>+1% per stack</style>) for every debuff your victim has.",
				lore: "It closes in...");

			GameData.UpdateItem(DLC1Content.Items.BearVoid,
				tier: ItemTier.VoidTier2,
				name: "Safer Spaces",
				pickup: "Block incoming damage. Recharges over time.",
				description: "<style=cIsHealth>Blocks</style> incoming damage once. Recharges after <style=cIsUtility>15 seconds</style> (<style=cStack>-2s per stack</style>). <style=cIsVoid>Corrupts all Innocent Eyes.</style>",
				lore: "See only the target.");

			// Yellow \\
			GameData.UpdateItem(RoR2Content.Items.BleedOnHitAndExplode,
				tier: ItemTier.Boss,
				name: "Shatterspleen",
				pickup: "Spread bleeding on kill.",
				description: "Bleed enemies on <style=cIsDamage>critical strike</style>. Upon death, bleeding enemies <style=cIsDamage>share their bleed stacks</style> with all enemies in a <style=cIsDamage>16m</style> radius (<style=cStack>+10m per stack</style>).",
				lore: "Nourish the earth.");

			// Red \\
			GameData.UpdateItem(RoR2Content.Items.Talisman,
				tier: ItemTier.Tier3,
				name: "Soulbound Catalyst",
				pickup: "Gain an equipment charge on Elite kill.",
				description: "Killing an <style=cIsUtility>Elite</style> restores <style=cIsUtility>1 equipment charge</style> (<style=cStack>+1 charge per stack</style>).",
				lore: "Let their kings and queens become fuel and fodder.");

			// Orange \\

			// Lunar \\

		}
	}
}