# Reconstituent

Reconstituent is an overhaul of Risk of Rain 2. It reduces or removes output-RNG, improves enemy aggression, unifies mechanics, and introduces a host of new elements (items, survivors, and so on).

Compatibility with other game-state mods is not a prime concern for early versions. Utility mods (like BetterUI) should work.

For more information, visit [the mod's subsite](https://fraudsclub.com/reconstituent).

# Setup

We recommend using [r2modman](https://thunderstore.io/package/ebkr/r2modman/) as your mod manager for Risk of Rain 2.

When it's released, you can download Reconstituent using r2modman's mod browser.

# Credits

## Personnel

- **Pr0nogo** - lead design
- **Veeq7** - programming, associate design, and testing
- **Mikemed** - testing
- **Groove_Salad, KingEnderBrine, Twiner, joseph, RandomlyAwesome, Anreol, iDeathHD** - API assistance

## Assets

- **Knockout Driver**
	- "Boxing Glove" (https://skfb.ly/6YPXK) by Pedro Perim is licensed under Creative Commons Attribution-ShareAlike (http://creativecommons.org/licenses/by-sa/4.0/).
- **Cosmic Carcass**
	- "Organic Armor Torso" (https://skfb.ly/ooIGD) by Ploobert is licensed under Creative Commons Attribution-NonCommercial (http://creativecommons.org/licenses/by-nc/4.0/).
- **Derelict Jawbone**
	- "Mandible CT" (https://skfb.ly/6TDUY) by Chair_Digital_Anatomy is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).

# Changelog

**0.0.3**

- *Caw.*